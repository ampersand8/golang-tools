FROM golang:1.15

RUN go get -u golang.org/x/lint/golint

RUN apt-get update && apt-get install -y --no-install-recommends \
    clang-7 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && ln -s /usr/bin/clang-7 /usr/bin/clang
# Set Clang as default CC
ENV set_clang /etc/profile.d/set-clang-cc.sh
RUN echo "export CC=clang-7" | tee -a ${set_clang} && chmod a+x ${set_clang}