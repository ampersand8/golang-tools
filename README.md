# golang-tools
A simple image based on the [original golang docker image](https://hub.docker.com/_/golang) with golint preinstalled.

# Build
```bash
docker build --tag golang-tools .
```

# Usage
## Interactive
You can use the shell and use it interactively.
```bash
docker run -it --rm golang-tools /bin/sh
> mkdir -p src/gitlab.com/amersand8/test
```

## Execute command
You can also simply run a command
```bash
docker run --rm golang-tools go version
```